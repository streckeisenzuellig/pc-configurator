package configurator;

import javafx.scene.control.Alert;

public abstract class AlertCreator {
    public static void createAlert(Alert.AlertType type, String title, String headerText, String content){
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(content);
        alert.showAndWait();
    }
}
