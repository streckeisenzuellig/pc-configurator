package configurator;

import configurator.controller.ConfigUIController;
import configurator.controller.MainUIController;
import configurator.controller.SelectUIController;
import configurator.data.IDataInterface;
import configurator.data.models.configuration.Configuration;
import configurator.data.models.types.ComponentTypes;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;

public class Main extends Application {

    private static Stage primaryStage;
    private static BorderPane rootLayout;
    public static IDataInterface dataHandler;

    private static final String ROOT_LAYOUT_FXML = "view/rootLayout.fxml";
    private static final String MAIN_UI_PATH = "view/mainUI.fxml";
    private static final String SELECT_UI_PATH = "view/selectUI.fxml";
    private static final String CONFIG_UI_PATH = "view/configUI.fxml";

    @Override
    public void start(Stage primaryStage) throws Exception{

        dataHandler = new configurator.data.MockupData();

        Main.primaryStage = primaryStage;
        Main.primaryStage.setTitle("PC-Configurator");

        initRootLayout();

        showMainUI();
    }

    /**
     * Initializes the root layout
     */
    private void initRootLayout(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class
                    .getResource(ROOT_LAYOUT_FXML));
            rootLayout = (BorderPane) loader.load();

            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            primaryStage.show();
        }
        catch (IOException ex){
            String content = "The rootLayout could not loaded!\n\n" + Arrays.toString(ex.getStackTrace());
            AlertCreator.createAlert(Alert.AlertType.ERROR, "Error", "", content);
        }
    }

    public static void showMainUI(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class
                    .getResource(MAIN_UI_PATH));
            AnchorPane mainUi = (AnchorPane) loader.load();
            MainUIController controller = loader.getController();
            controller.initUI();
            rootLayout.setCenter(mainUi);
        }
        catch (IOException ex){
            ex.printStackTrace();
            String content = "The mainUI could not be loaded!\n\n" + Arrays.toString(ex.getStackTrace());
            AlertCreator.createAlert(Alert.AlertType.ERROR, "Error", "", content);
        }
    }

    public static void showConfigUI(Configuration configuration){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class
                    .getResource(CONFIG_UI_PATH));
            AnchorPane configUi = (AnchorPane) loader.load();
            ConfigUIController controller = loader.getController();
            if (configuration != null){
                controller.setConfiguration(configuration);
            }
            controller.initUI();
            rootLayout.setCenter(configUi);
        }
        catch (IOException ex){
            ex.printStackTrace();
            String content = "The configUI could not be loaded!\n\n" + Arrays.toString(ex.getStackTrace());
            AlertCreator.createAlert(Alert.AlertType.ERROR, "Error", "", content);
        }
    }

    public static void showSelectUI(Configuration configuration, ComponentTypes componentType){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class
                    .getResource(SELECT_UI_PATH));
            AnchorPane selectUi = (AnchorPane) loader.load();
            SelectUIController controller = loader.getController();
            controller.setConfiguration(configuration);
            controller.initUI(componentType);
            rootLayout.setCenter(selectUi);
        }
        catch (IOException ex){
            ex.printStackTrace();
            String content = "The selectUI could not be loaded!\n\n" + Arrays.toString(ex.getStackTrace());
            AlertCreator.createAlert(Alert.AlertType.ERROR, "Error", "", content);
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
