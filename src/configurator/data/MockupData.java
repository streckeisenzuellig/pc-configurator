package configurator.data;

import configurator.data.models.configuration.components.*;
import configurator.data.models.types.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lukas on 07.04.2017.
 */
public class MockupData implements IDataInterface {

    public List<Case> getCases(){
        ArrayList<Case> Cases = new ArrayList<>();
        Case myCase = new Case();
        myCase.setDriveShafts(new HashMap<StorageMediumSizeTypes, Integer>(){            {
                put(StorageMediumSizeTypes._3_5_inch, 6);
                put(StorageMediumSizeTypes._2_5_inch, 1);
            }});
        myCase.setFormFactor(FormFactorTypes.ATX);
        myCase.setHeight(53.50);
        myCase.setLength(58);
        myCase.setWidth(22);
        myCase.setMaterial("Stahl/Steel");
        myCase.setMaxCPUCoolingHeight(177);
        myCase.setMaxGraphicCardLength(344);
        myCase.setWaterCoolingPossible(true);
        myCase.setMaxPowerAdapterLength(35);
        myCase.setManufacturer("Thermaltake");
        myCase.setProductName("Overseer RX-I (VN700M1W2N) Window (Big Tower)");

        Cases.add(myCase);

        return Cases;

    }

    public List<CPU> getCPUs(){
        ArrayList<CPU> CPUs =  new ArrayList<CPU>();

        CPU cpu =  new CPU(){};
        cpu.setCpuBase(CPUBaseTypes.LGA_2011_v3);
        cpu.setCpuClockFrequency("3 GHz");
        cpu.setMaxCpuClockFrequency("3.5 GHz");
        cpu.setNumberOfThreads(20);
        cpu.setNumberOfCpuCores(10);
        cpu.setMaxThermalDesignPower("140 W");
        cpu.setProductName("Intel Core i7 6950X");
        cpu.setDescription("40-lane PCI-Express support (x16 + x16 + x8)");
        cpu.setManufacturer("Intel");

        CPUs.add(cpu);
        return CPUs;
    }

    public List<CPUCooling> getCPUCoolings(){
        ArrayList<CPUCooling>  CPUCoolings =  new ArrayList<CPUCooling>();

        CPUCooling cpucooling =  new CPUCooling(){};
        cpucooling.setManufacturer("Cooler Master");
        cpucooling.setDescription("Cooler Master Wasserkühlung MasterLiquid Pro 120");
        cpucooling.setProductName("MasterLiquid Pro 120");
        cpucooling.setCompatibleCpuBases(new ArrayList<CPUBaseTypes>() {{
                add(CPUBaseTypes.LGA_2011_v3);
                add(CPUBaseTypes.LGA_2011);
                add(CPUBaseTypes.LGA_1150);
                add(CPUBaseTypes.LGA_1151);
                add(CPUBaseTypes.LGA_1155);
                add(CPUBaseTypes.AM3_PLUS);
                add(CPUBaseTypes.FM2_PLUS);
            }});
        cpucooling.setHeight(25);
        cpucooling.setWidth(120);
        cpucooling.setLength(120);
        cpucooling.setVoltage("12 V");
        cpucooling.setPowerConsumption("6 W");

        CPUCoolings.add(cpucooling);
        return CPUCoolings;
    }

    public List<GraphicCard> getGraphicCards(){
        ArrayList<GraphicCard>  graphicCards =  new ArrayList<GraphicCard>();

        GraphicCard graphicCard =  new GraphicCard(){};
        graphicCard.setProductName("GeForce GTX 1080 Ti STRIX O11G-GAMING");
        graphicCard.setManufacturer("Asus");
        graphicCard.setDirectXVersion("12");
        graphicCard.setGraphicCpuBoostFrequency("1708 MHz");
        graphicCard.setGraphicCpuClockFrequency("1569 MHz");
        graphicCard.setGraphicMemoryCapacity("11 GB");
        graphicCard.setGraphicMemoryType("GDDR5X");
        graphicCard.setMinPowerAdapterPower("500 W");
        graphicCard.setHeight(5.25);
        graphicCard.setWidth(13.4);
        graphicCard.setLength(29.8);
        graphicCard.setMonitorInterfaces(new HashMap<ConnectionInterfaceTypes, Integer>(){{
            put(ConnectionInterfaceTypes.HDMI, 2);
            put(ConnectionInterfaceTypes.DisplayPort, 2);
            put(ConnectionInterfaceTypes.DVI_D, 1);
        }
        });
        graphicCard.setPowerSupplyInterfaces(new HashMap<ConnectionInterfaceTypes, Integer>(){{
            put(ConnectionInterfaceTypes.PCI_E_8_pin, 2);
        }});
        graphicCard.setPowerConsumption("250 W");

        graphicCards.add(graphicCard);
        return graphicCards;
    }

    public List<Mainboard> getMainboards(){
        ArrayList<Mainboard>  mainboards =  new ArrayList<Mainboard>();

        Mainboard mainboard =  new Mainboard(){};

        mainboard.setProductName("RAMPAGE V EXTREME/U3.1");
        mainboard.setCpuBase(CPUBaseTypes.LGA_2011_v3);
        mainboard.setFormFactor(FormFactorTypes.E_ATX);
        mainboard.setLength(30.50);
        mainboard.setWidth(27.20);
        mainboard.setRamType(RAMTypes.DDR4_RAM);
        mainboard.setRamFormFactor(RAMFormFactorTypes.DIMM_288);
        mainboard.setManufacturer("ASUS");
        mainboard.setDescription("\n" + "Komplettes Zubehör: Manual\n" + "I/O Shield\n" + "10 x SATA 6Gb/s cable(s)\n");
        mainboard.setMainboardInterfaces(new HashMap<ConnectionInterfaceTypes, Integer>(){{
            put(ConnectionInterfaceTypes.S_ATA_III, 8);
            put(ConnectionInterfaceTypes.PCI_E_x16, 5);
            put(ConnectionInterfaceTypes.PCI_E_x1, 1);
            put(ConnectionInterfaceTypes.PS_2_COMBO, 1);
            put(ConnectionInterfaceTypes.USB_3_0, 10);
        }});

        mainboards.add(mainboard);

        return mainboards;
    }

    public List<Monitor> getMonitors() {
        return null;
    }

    public List<PowerAdapter> getPowerAdapters(){
        ArrayList<PowerAdapter>  poweradapters =  new ArrayList<PowerAdapter>();

        PowerAdapter poweradapter =  new PowerAdapter(){};
        poweradapter.setFormFactor(FormFactorTypes.ATX);
        poweradapter.setHeight(8.50 );
        poweradapter.setLength(16.50);
        poweradapter.setWidht(15);
        poweradapter.setWeight(2300);
        poweradapter.setDescription("");
        poweradapter.setManufacturer("EVGA");
        poweradapter.setProductName("SuperNOVA G2");
        poweradapter.setPower("750 W");
        poweradapter.setPowerAdapterInterfaces(new HashMap<ConnectionInterfaceTypes, Integer>(){{
            put(ConnectionInterfaceTypes._24_pin_ATX, 2);
            put(ConnectionInterfaceTypes._8_pin_ATX_12V, 2);
        }});

        poweradapters.add(poweradapter);

        return poweradapters;
    }

    public List<RAM> getRAMs(){
        ArrayList<RAM>  rams =  new ArrayList<RAM>();

        RAM ram =  new RAM(){};
        ram.setHeight(34.04);
        ram.setRamFormFactor(RAMFormFactorTypes.DIMM_288);
        ram.setRamType(RAMTypes.DDR4_RAM);
        ram.setVoltage("1.20 V");
        ram.setDescription("HyperX FURY DDR4 Memory 16GB 2-Kit 2133MHz, Arbeitsspeicher Bauform: DIMM, Arbeitsspeicher-Typ: DDR4, Arbeitsspeicher Geschwindigkeit: 2133 MHz, Arbeitsspeicher Pins: 288, Fehlerkorrektur: Unbuffered, Anzahl Speichermodule Kit: 2, Speicherkapazität pro Modul: 8 GB.");
        ram.setManufacturer("HyperX");
        ram.setProductName("Fury");
        rams.add(ram);

        return rams;
    }

    public List<StorageMedium> getStorageMediums(){
        ArrayList<StorageMedium>  storageMedia =  new ArrayList<StorageMedium>();

        StorageMedium storageMedium =  new StorageMedium(){};
        storageMedium.setInterfaceType(ConnectionInterfaceTypes.S_ATA_III);
        storageMedium.setPowerConsumptionWhileStandby("0.40 W");
        storageMedium.setPowerConsumptionWhileUse("3.30 W");
        storageMedium.setSizeType(StorageMediumSizeTypes._2_5_inch);
        storageMedium.setStorageSpace("1024 GB");
        storageMedium.setStorageMediumType(StorageMediumTypes.SSD);
        storageMedium.setDescription("Samsung SSD 850 PRO 1 TB 2.5\", Speicherkapazität total: 1 TB, Speicherschnittstelle: S-ATA III (6Gb/s), SSD Bauhöhe: 6.8 mm, SSD Formfaktor: 2,5.");
        storageMedium.setManufacturer("Samsung");
        storageMedium.setProductName("850 Pro");

        storageMedia.add(storageMedium);

        return storageMedia;
    }

    public List<WindowsVersion> getWindowsVersions(){
        ArrayList<WindowsVersion>  windowsVersions =  new ArrayList<WindowsVersion>();

        WindowsVersion windowsVersion =  new WindowsVersion();
        windowsVersion.setProductName("Windows 10 Home");
        windowsVersion.setDescription("Beim Freilegen des Key's ist vorsicht geboten, unkenntliche Keys können nicht wiederhergestellt werden!\n" + "\n" + "Alles auf Start: Das vertraute Startmenü ist zurück und es ist besser geworden – mit neuen Funktionalitäten und Anpassungsmöglichkeiten.\n" + "Sprachsteuerung, Stifteingabe und Touch: Du entscheidest, wie du am besten arbeiten kannst.\n" + "Noch sicherer: Windows 10 ist das bislang sicherste Windows, inklusive regelmässiger Updates.\n" + "Microsoft Edge: Schreibe, tippe und zeichne mit Web Note direkt auf Webseiten.");
        windowsVersion.setManufacturer("Microsoft");
        windowsVersion.setWindowsVersion("Windows 10");
        windowsVersion.setLicenceType(WindowsLicenceTypes.HOME);

        windowsVersions.add(windowsVersion);

        return windowsVersions;
    }

}