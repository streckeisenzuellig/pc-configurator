package configurator.data;

import configurator.data.models.configuration.components.*;

import java.util.List;

/**
 * Created by lukas on 05.05.2017.
 */
public interface IDataInterface {

    List<Case> getCases();

    List<CPU> getCPUs();

    List<CPUCooling> getCPUCoolings();

    List<GraphicCard> getGraphicCards();

    List<Mainboard> getMainboards();

    List<Monitor> getMonitors();

    List<PowerAdapter> getPowerAdapters();

    List<RAM> getRAMs();

    List<StorageMedium> getStorageMediums();List<WindowsVersion> getWindowsVersions();
}
