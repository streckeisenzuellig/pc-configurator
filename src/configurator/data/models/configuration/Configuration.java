package configurator.data.models.configuration;

import configurator.data.models.configuration.components.*;

/**
 * Created by Lukas_Streckeisen on 21.04.2017.
 */
public class Configuration {

    private Case selectedCase;

    private CPU selectedCPU;

    private CPUCooling selectedCPUCooling;

    private GraphicCard selectedGraphicCard;

    private Mainboard selectedMainboard;

    private Monitor selectedMonitor;

    private PowerAdapter selectedPowerAdapter;

    private RAM selectedRAM;

    private StorageMedium selectedHDD;

    private StorageMedium selectedSSD;

    private WindowsVersion selectedWindowsVersion;

    private String configurationName;

    public Configuration(){
        selectedCase = new Case();
        selectedCPU = new CPU();
        selectedCPUCooling = new CPUCooling();
        selectedGraphicCard = new GraphicCard();
        selectedHDD = new StorageMedium();
        selectedMainboard = new Mainboard();
        selectedMonitor = new Monitor();
        selectedPowerAdapter = new PowerAdapter();
        selectedRAM = new RAM();
        selectedSSD = new StorageMedium();
        selectedWindowsVersion = new WindowsVersion();
    }

    //Getters


    public Case getSelectedCase() {
        return selectedCase;
    }

    public CPU getSelectedCPU() {
        return selectedCPU;
    }

    public CPUCooling getSelectedCPUCooling() {
        return selectedCPUCooling;
    }

    public GraphicCard getSelectedGraphicCard() {
        return selectedGraphicCard;
    }

    public Mainboard getSelectedMainboard() {
        return selectedMainboard;
    }

    public Monitor getSelectedMonitor() {
        return selectedMonitor;
    }

    public PowerAdapter getSelectedPowerAdapter() {
        return selectedPowerAdapter;
    }

    public RAM getSelectedRAM() {
        return selectedRAM;
    }

    public StorageMedium getSelectedHDD() {
        return selectedHDD;
    }

    public StorageMedium getSelectedSSD() {
        return selectedSSD;
    }

    public WindowsVersion getSelectedWindowsVersion() {
        return selectedWindowsVersion;
    }

    public String getConfigurationName() {
        return configurationName;
    }

    //Setters

    public void setSelectedCase(Case selectedCase) {
        this.selectedCase = selectedCase;
    }

    public void setSelectedCPU(CPU selectedCPU) {
        this.selectedCPU = selectedCPU;
    }

    public void setSelectedCPUCooling(CPUCooling selectedCPUCooling) {
        this.selectedCPUCooling = selectedCPUCooling;
    }

    public void setSelectedGraphicCard(GraphicCard selectedGraphicCard) {
        this.selectedGraphicCard = selectedGraphicCard;
    }

    public void setSelectedMainboard(Mainboard selectedMainboard) {
        this.selectedMainboard = selectedMainboard;
    }

    public void setSelectedHDD(StorageMedium selectedHDD) {
        this.selectedHDD = selectedHDD;
    }

    public void setSelectedMonitor(Monitor selectedMonitor) {
        this.selectedMonitor = selectedMonitor;
    }

    public void setSelectedPowerAdapter(PowerAdapter selectedPowerAdapter) {
        this.selectedPowerAdapter = selectedPowerAdapter;
    }

    public void setSelectedRAM(RAM selectedRAM) {
        this.selectedRAM = selectedRAM;
    }

    public void setSelectedSSD(StorageMedium selectedSSD) {
        this.selectedSSD = selectedSSD;
    }

    public void setSelectedWindowsVersion(WindowsVersion selectedWindowsVersion) {
        this.selectedWindowsVersion = selectedWindowsVersion;
    }

    public void setConfigurationName(String configurationName) {
        this.configurationName = configurationName;
    }
}
