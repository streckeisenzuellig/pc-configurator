package configurator.data.models.configuration.components;

import configurator.data.models.types.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lukas on 07.04.2017.
 */
public class Mainboard extends Product{

    private FormFactorTypes formFactor;
    private CPUBaseTypes cpuBase;
    private RAMTypes ramType;
    private RAMFormFactorTypes ramFormFactor;
    private HashMap<ConnectionInterfaceTypes, Integer> mainboardInterfaces;
    private double length;
    private double width;

    public Mainboard(){
        super();
    }

    @Override
    public String getSpecifications(){
        StringBuilder specifications = new StringBuilder(
                "Produktname: " + getProductName() + "\n"
                + "Hersteller: " + getManufacturer() + "\n"
                + "CPU-Sockel: " + cpuBase.name() + "\n"
                + "Formfaktor: " + formFactor.name() + "\n"
                + "CPU-Sockel: " + cpuBase.name() + "\n"
                + "RAM-Typ: " + ramType.name() + "\n"
                + "RAM-Formfaktor: " + ramFormFactor.name() + "\n"
                + "Mainboard-Anschlüsse: \n"
        );

        for (Object o : mainboardInterfaces.entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            specifications.append("      ").append(pair.getKey()).append(": ").append(pair.getValue()).append("\n");
        }

        specifications.append("Länge: ").append(length).append(" cm\n");
        specifications.append("Breite: ").append(width).append(" cm\n");

        return specifications.toString();
    }

    //Getters


    public double getWidth() {
        return width;
    }

    public CPUBaseTypes getCpuBase() {
        return cpuBase;
    }

    public FormFactorTypes getFormFactor() {
        return formFactor;
    }

    public double getLength() {
        return length;
    }

    public HashMap<ConnectionInterfaceTypes, Integer> getMainboardInterfaces() {
        return mainboardInterfaces;
    }

    public RAMFormFactorTypes getRamFormFactor() {
        return ramFormFactor;
    }

    public RAMTypes getRamType() {
        return ramType;
    }

    //Setters

    public void setWidth(double width) {
        this.width = width;
    }

    public void setCpuBase(CPUBaseTypes cpuBase) {
        this.cpuBase = cpuBase;
    }

    public void setFormFactor(FormFactorTypes formFactor) {
        this.formFactor = formFactor;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setMainboardInterfaces(HashMap<ConnectionInterfaceTypes, Integer> mainboardInterfaces) {
        this.mainboardInterfaces = mainboardInterfaces;
    }

    public void setRamFormFactor(RAMFormFactorTypes ramFormFactor) {
        this.ramFormFactor = ramFormFactor;
    }

    public void setRamType(RAMTypes ramType) {
        this.ramType = ramType;
    }
}
