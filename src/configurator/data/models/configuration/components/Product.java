package configurator.data.models.configuration.components;

/**
 * Created by lukas on 07.04.2017.
 */
public abstract class Product {
    private String productName;
    private String manufacturer;
    private String description;
    private String imagePath;

    public Product(){

    }

    public String getSpecifications(){
        return "Product Name: " + productName + "\n"
                + "Manufacturer: " + manufacturer + "\n";
    }

    @Override
    public String toString(){
        return manufacturer + " " + productName;
    }

    //Getters
    public String getProductName() {
        return productName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getDescription() {
        return description;
    }

    public String getImagePath() {
        return imagePath;
    }

    //Setters

    public void setDescription(String description) {
        this.description = description;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
