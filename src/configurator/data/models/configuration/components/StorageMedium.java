package configurator.data.models.configuration.components;

import configurator.data.models.types.ConnectionInterfaceTypes;
import configurator.data.models.types.StorageMediumSizeTypes;
import configurator.data.models.types.StorageMediumTypes;

/**
 * Created by lukas on 07.04.2017.
 */
public class StorageMedium extends Product {

    private ConnectionInterfaceTypes interfaceType;
    private String powerConsumptionWhileUse;
    private String powerConsumptionWhileStandby;
    private StorageMediumTypes storageMediumType;
    private StorageMediumSizeTypes sizeType;
    private String storageSpace;


    public StorageMedium(){
        super();
    }

    @Override
    public String getSpecifications(){
        StringBuilder specifications = new StringBuilder(
                "Produktname: " + getProductName() + "\n"
                + "Hersteller: " + getManufacturer() + "\n"
                + "Schnittstelle: " + interfaceType.name() + "\n"
                + "Stromverbrauch Betrieb: " + powerConsumptionWhileUse + "\n"
                + "Stromverbrauch Standby: " + powerConsumptionWhileStandby + "\n"
                + "Grösse: " + sizeType.name() + " Zoll\n"
                + "Speicherplatz: " + storageSpace + "\n"
        );
        return specifications.toString();
    }

    //Getters

    public StorageMediumTypes getStorageMediumType() {
        return storageMediumType;
    }

    public ConnectionInterfaceTypes getInterfaceType() {
        return interfaceType;
    }

    public StorageMediumSizeTypes getSizeType() {
        return sizeType;
    }

    public String getPowerConsumptionWhileStandby() {
        return powerConsumptionWhileStandby;
    }

    public String getPowerConsumptionWhileUse() {
        return powerConsumptionWhileUse;
    }

    public String getStorageSpace() {
        return storageSpace;
    }

    //Setters

    public void setStorageMediumType(StorageMediumTypes storageMediumType) {
        this.storageMediumType = storageMediumType;
    }

    public void setInterfaceType(ConnectionInterfaceTypes interfaceType) {
        this.interfaceType = interfaceType;
    }

    public void setPowerConsumptionWhileStandby(String powerConsumptionWhileStandby) {
        this.powerConsumptionWhileStandby = powerConsumptionWhileStandby;
    }

    public void setPowerConsumptionWhileUse(String powerConsumptionWhileUse) {
        this.powerConsumptionWhileUse = powerConsumptionWhileUse;
    }

    public void setSizeType(StorageMediumSizeTypes sizeType) {
        this.sizeType = sizeType;
    }

    public void setStorageSpace(String storageSpace) {
        this.storageSpace = storageSpace;
    }
}
