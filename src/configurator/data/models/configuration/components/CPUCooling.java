package configurator.data.models.configuration.components;

import configurator.data.models.types.CPUBaseTypes;
import configurator.data.models.types.ConnectionInterfaceTypes;

import java.util.List;

/**
 * Created by lukas on 07.04.2017.
 */
public class CPUCooling extends Product {

    private List<CPUBaseTypes> compatibleCpuBases;
    private double width;
    private double length;
    private double height;
    private String powerConsumption;
    private String voltage;
    private ConnectionInterfaceTypes connectionInterface;

    public CPUCooling(){
        super();
    }

    @Override
    public String getSpecifications(){
        StringBuilder specifications = new StringBuilder(
                "Produktname: " + getProductName() + "\n"
                + "Hersteller: " + getManufacturer() + "\n"
                + "Kompatible CPU-Sockel: \n"
        );

        for (CPUBaseTypes cbt: compatibleCpuBases){
            specifications.append("     ").append(cbt.name()).append(", ");
        }
        specifications.append("\n");
        specifications.append("Stromversorgungsanschluss: ").append(connectionInterface.name()).append("\n");
        specifications.append("Stromverbrach: ").append(powerConsumption).append("\n");
        specifications.append("Spannung: ").append(voltage).append("\n");
        specifications.append("Breite: ").append(width).append(" cm\n");
        specifications.append("Länge: ").append(length).append(" cm\n");
        specifications.append("Höhe: ").append(height).append(" cm\n");

        return specifications.toString();
    }

    //Getters

    public double getHeight() {
        return height;
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public ConnectionInterfaceTypes getConnectionInterface() {
        return connectionInterface;
    }

    public List<CPUBaseTypes> getCompatibleCpuBases() {
        return compatibleCpuBases;
    }

    public String getPowerConsumption() {
        return powerConsumption;
    }

    public String getVoltage() {
        return voltage;
    }

    //Setters

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setCompatibleCpuBases(List<CPUBaseTypes> compatibleCpuBases) {
        this.compatibleCpuBases = compatibleCpuBases;
    }

    public void setConnectionInterface(ConnectionInterfaceTypes connectionInterface) {
        this.connectionInterface = connectionInterface;
    }

    public void setPowerConsumption(String powerConsumption) {
        this.powerConsumption = powerConsumption;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }
}
