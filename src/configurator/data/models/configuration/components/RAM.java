package configurator.data.models.configuration.components;

import configurator.data.models.types.RAMFormFactorTypes;
import configurator.data.models.types.RAMTypes;

/**
 * Created by lukas on 07.04.2017.
 */
public class RAM extends Product{
    private RAMTypes ramType;
    private RAMFormFactorTypes ramFormFactor;
    private String voltage;
    private double height;

    public RAM(){
        super();
    }

    @Override
    public String getSpecifications(){
        StringBuilder specifications = new StringBuilder(
                "Produktname: " + getProductName() + "\n"
                + "Hersteller: " + getManufacturer() + "\n"
                + "RAM-Typ: " + ramType.name() + "\n"
                + "RAM-Formfaktor: " + ramFormFactor.name() + "\n"
                + "Spannung: " + voltage + "\n"
                + "Höhe: " + height + " mm\n"
        );
        return specifications.toString();
    }

    //Getters

    public double getHeight() {
        return height;
    }

    public RAMTypes getRamType() {
        return ramType;
    }

    public RAMFormFactorTypes getRamFormFactor() {
        return ramFormFactor;
    }

    public String getVoltage() {
        return voltage;
    }

    //Setters

    public void setHeight(double height) {
        this.height = height;
    }

    public void setRamType(RAMTypes ramType) {
        this.ramType = ramType;
    }

    public void setRamFormFactor(RAMFormFactorTypes ramFormFactor) {
        this.ramFormFactor = ramFormFactor;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }
}
