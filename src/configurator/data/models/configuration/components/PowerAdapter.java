package configurator.data.models.configuration.components;

import configurator.data.models.types.ConnectionInterfaceTypes;
import configurator.data.models.types.FormFactorTypes;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lukas on 07.04.2017.
 */
public class PowerAdapter extends Product {

    private FormFactorTypes formFactor;
    private HashMap<ConnectionInterfaceTypes, Integer> powerAdapterInterfaces;
    private String power;
    private double length;
    private double widht;
    private double height;
    private int weight;

    @Override
    public String getSpecifications(){
        StringBuilder specifications = new StringBuilder(
                "Produktname: " + getProductName() + "\n"
                + "Hersteller: " + getManufacturer() + "\n"
                + "Formfaktor: " + formFactor.name() + "\n"
                + "Leistung: " + power + "\n"
                + "Anschlüsse: \n"
        );

        for (Object o : powerAdapterInterfaces.entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            specifications.append("      ").append(pair.getKey()).append(": ").append(pair.getValue()).append("\n");
        }

        specifications.append("Länge: ").append(length).append("\n");
        specifications.append("Breite: ").append(widht).append("\n");
        specifications.append("Höhe: ").append(height).append("\n");
        specifications.append("Gewicht: ").append(weight).append(" g\n");

        return specifications.toString();
    }

    public PowerAdapter(){
        super();
    }

    //Getters


    public double getLength() {
        return length;
    }

    public FormFactorTypes getFormFactor() {
        return formFactor;
    }

    public double getHeight() {
        return height;
    }

    public double getWidht() {
        return widht;
    }

    public HashMap<ConnectionInterfaceTypes, Integer> getPowerAdapterInterfaces() {
        return powerAdapterInterfaces;
    }

    public int getWeight() {
        return weight;
    }

    public String getPower() {
        return power;
    }

    //Setters

    public void setLength(double length) {
        this.length = length;
    }

    public void setFormFactor(FormFactorTypes formFactor) {
        this.formFactor = formFactor;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public void setPowerAdapterInterfaces(HashMap<ConnectionInterfaceTypes, Integer> powerAdapterInterfaces) {
        this.powerAdapterInterfaces = powerAdapterInterfaces;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setWidht(double widht) {
        this.widht = widht;
    }
}
