package configurator.data.models.configuration.components;

import configurator.data.models.types.ConnectionInterfaceTypes;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lukas on 07.04.2017.
 */
public class GraphicCard extends Product {
    private String graphicCpuClockFrequency;
    private String graphicCpuBoostFrequency;
    private String graphicMemoryType;
    private String graphicMemoryCapacity;
    private String directXVersion;
    private HashMap<ConnectionInterfaceTypes, Integer> monitorInterfaces;
    private String powerConsumption;
    private String minPowerAdapterPower;
    private HashMap<ConnectionInterfaceTypes, Integer> powerSupplyInterfaces;
    private double length;
    private double width;
    private double height;

    public GraphicCard(){
        super();
    }

    @Override
    public String getSpecifications(){
        StringBuilder specifications = new StringBuilder(
                "Produktname: " + getProductName() + "\n"
                + "Hersteller: " + getManufacturer() + "\n"
                + "Grafik-CPU Taktfrequenz" + graphicCpuClockFrequency + "\n"
                + "Grafik-CPU Boost-Taktfrequenz" + graphicCpuBoostFrequency + "\n"
                + "Grafikspeicher Kapazität: " + graphicMemoryCapacity + "\n"
                + "Grafikspeicher Type: " + graphicMemoryType + "\n"
                + "DirectX-Version: " + directXVersion + "\n"
                + "Monitor-Anschlüsse: \n"
        );

        for (Object o : monitorInterfaces.entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            specifications.append("      ").append(pair.getKey()).append(": ").append(pair.getValue()).append("\n");
        }

        specifications.append("Stromverbrauch: ").append(powerConsumption).append("\n");
        specifications.append("Min. Netzteil-Leistung: ").append(minPowerAdapterPower).append("\n");
        specifications.append("Stromversorgung: ").append("\n");

        for (Object o : powerSupplyInterfaces.entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            specifications.append("      ").append(pair.getKey()).append(": ").append(pair.getValue()).append("\n");
        }

        specifications.append("Länge: ").append(length).append(" cm\n");
        specifications.append("Breite: ").append(width).append(" cm\n");
        specifications.append("Höhe: ").append(height).append(" cm\n");

        return specifications.toString();
    }

    //Getters

    public String getPowerConsumption() {
        return powerConsumption;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public double getHeight() {
        return height;
    }

    public HashMap<ConnectionInterfaceTypes, Integer> getMonitorInterfaces() {
        return monitorInterfaces;
    }

    public HashMap<ConnectionInterfaceTypes, Integer> getPowerSupplyInterfaces() {
        return powerSupplyInterfaces;
    }

    public String getDirectXVersion() {
        return directXVersion;
    }

    public String getGraphicCpuBoostFrequency() {
        return graphicCpuBoostFrequency;
    }

    public String getGraphicCpuClockFrequency() {
        return graphicCpuClockFrequency;
    }

    public String getGraphicMemoryCapacity() {
        return graphicMemoryCapacity;
    }

    public String getGraphicMemoryType() {
        return graphicMemoryType;
    }

    public String getMinPowerAdapterPower() {
        return minPowerAdapterPower;
    }

    //Setters

    public void setPowerConsumption(String powerConsumption) {
        this.powerConsumption = powerConsumption;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setDirectXVersion(String directXVersion) {
        this.directXVersion = directXVersion;
    }

    public void setGraphicCpuBoostFrequency(String graphicCpuBoostFrequency) {
        this.graphicCpuBoostFrequency = graphicCpuBoostFrequency;
    }

    public void setGraphicCpuClockFrequency(String graphicCpuClockFrequency) {
        this.graphicCpuClockFrequency = graphicCpuClockFrequency;
    }

    public void setGraphicMemoryCapacity(String graphicMemoryCapacity) {
        this.graphicMemoryCapacity = graphicMemoryCapacity;
    }

    public void setGraphicMemoryType(String graphicMemoryType) {
        this.graphicMemoryType = graphicMemoryType;
    }

    public void setMinPowerAdapterPower(String minPowerAdapterPower) {
        this.minPowerAdapterPower = minPowerAdapterPower;
    }

    public void setMonitorInterfaces(HashMap<ConnectionInterfaceTypes, Integer> monitorInterfaces) {
        this.monitorInterfaces = monitorInterfaces;
    }

    public void setPowerSupplyInterfaces(HashMap<ConnectionInterfaceTypes, Integer> powerSupplyInterfaces) {
        this.powerSupplyInterfaces = powerSupplyInterfaces;
    }
}
