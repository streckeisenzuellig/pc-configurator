package configurator.data.models.configuration.components;

import configurator.data.models.types.FormFactorTypes;
import configurator.data.models.types.StorageMediumSizeTypes;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lukas on 07.04.2017.
 */
public class Case extends Product {

    private FormFactorTypes formFactor;
    private boolean isWaterCoolingPossible;
    private int maxGraphicCardLength;
    private int maxCPUCoolingHeight;
    private int maxPowerAdapterLength;
    private HashMap<StorageMediumSizeTypes, Integer> driveShafts;
    private double width;
    private double length;
    private double height;
    private String material;

    public Case(){
        super();
    }

    @Override
    public String getSpecifications(){
        StringBuilder specifications = new StringBuilder(
                "Produktname: " + getProductName() + "\n"
                + "Hersteller: " + getManufacturer() + "\n"
                + "Formfaktor: " + formFactor.name() + "\n"
                + "Material: " + material + "\n"
                + "Maximale Grafikkartenlänge: " + maxGraphicCardLength + " cm\n"
                + "Maximale CPU-Kühlerhöhe: " + maxCPUCoolingHeight + " cm\n"
                + "Maximale Netzteillänge: " + maxPowerAdapterLength + " cm\n"
                + "Laufwerkplätze: \n");

        for (Object o : driveShafts.entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            specifications.append("      ").append(pair.getKey()).append(": ").append(pair.getValue()).append("\n");
        }

        specifications.append("Wasserkühlung möglich: ").append(isWaterCoolingPossible).append("\n");
        specifications.append("Höhe: ").append(height).append("\n");
        specifications.append("Breite: ").append(width).append("\n");
        specifications.append("Länge: ").append(length).append("\n");
        return specifications.toString();
    }

    //Getters
    public FormFactorTypes getFormFactor() {
        return formFactor;
    }

    public boolean isWaterCoolingPossible() {
        return isWaterCoolingPossible;
    }

    public int getMaxGraphicCardLength() {
        return maxGraphicCardLength;
    }

    public int getMaxCPUCoolingHeight() {
        return maxCPUCoolingHeight;
    }

    public int getMaxPowerAdapterLength() {
        return maxPowerAdapterLength;
    }

    public HashMap<StorageMediumSizeTypes, Integer> getDriveShafts() {
        return driveShafts;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public double getHeight() {
        return height;
    }

    public String getMaterial() {
        return material;
    }

    //Setters
    public void setDriveShafts(HashMap<StorageMediumSizeTypes, Integer> driveShafts) {
        this.driveShafts = driveShafts;
    }

    public void setFormFactor(FormFactorTypes formFactor) {
        this.formFactor = formFactor;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setMaxCPUCoolingHeight(int maxCPUCoolingHeight) {
        this.maxCPUCoolingHeight = maxCPUCoolingHeight;
    }

    public void setMaxGraphicCardLength(int maxGraphicCardLength) {
        this.maxGraphicCardLength = maxGraphicCardLength;
    }

    public void setMaxPowerAdapterLength(int maxPowerAdapterLength) {
        this.maxPowerAdapterLength = maxPowerAdapterLength;
    }

    public void setWaterCoolingPossible(boolean waterCoolingPossible) {
        isWaterCoolingPossible = waterCoolingPossible;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setMaterial(String material) {
        this.material = material;
    }
}
