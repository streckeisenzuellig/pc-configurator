package configurator.data.models.configuration.components;

import configurator.data.models.types.CPUBaseTypes;

/**
 * Created by lukas on 07.04.2017.
 */
public class CPU extends Product{
    private CPUBaseTypes cpuBase;
    private String cpuClockFrequency;
    private String maxCpuClockFrequency;
    private int numberOfCpuCores;
    private int numberOfThreads;
    private String maxThermalDesignPower;

    public CPU(){
        super();
    }

    @Override
    public String getSpecifications(){
        StringBuilder specifications = new StringBuilder(
                "Produktname: " + getProductName() + "\n"
                + "Hersteller: " + getManufacturer() + "\n"
                + "CPU-Sockel: " + cpuBase.name() + "\n"
                + "Taktfrequenz: " + cpuClockFrequency + "\n"
                + "Turbo Taktfrequenz: " + maxCpuClockFrequency + "\n"
                + "Anzahl Cores: " + numberOfCpuCores + "\n"
                + "Anzahl Threads: " + numberOfThreads + "\n"
                + "TDP : " + maxThermalDesignPower + "\n"
        );
        return specifications.toString();
    }

    //Getters

    public CPUBaseTypes getCpuBase() {
        return cpuBase;
    }

    public int getNumberOfCpuCores() {
        return numberOfCpuCores;
    }

    public int getNumberOfThreads() {
        return numberOfThreads;
    }

    public String getCpuClockFrequency() {
        return cpuClockFrequency;
    }

    public String getMaxCpuClockFrequency() {
        return maxCpuClockFrequency;
    }

    public String getMaxThermalDesignPower() {
        return maxThermalDesignPower;
    }

    //Setters

    public void setCpuBase(CPUBaseTypes cpuBase) {
        this.cpuBase = cpuBase;
    }

    public void setCpuClockFrequency(String cpuClockFrequency) {
        this.cpuClockFrequency = cpuClockFrequency;
    }

    public void setMaxCpuClockFrequency(String maxCpuClockFrequency) {
        this.maxCpuClockFrequency = maxCpuClockFrequency;
    }

    public void setMaxThermalDesignPower(String maxThermalDesignPower) {
        this.maxThermalDesignPower = maxThermalDesignPower;
    }

    public void setNumberOfCpuCores(int numberOfCpuCores) {
        this.numberOfCpuCores = numberOfCpuCores;
    }

    public void setNumberOfThreads(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }
}
