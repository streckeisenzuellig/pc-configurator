package configurator.data.models.configuration.components;

import configurator.data.models.types.WindowsLicenceTypes;

/**
 * Created by lukas on 07.04.2017.
 */
public class WindowsVersion extends Product{

    private String windowsVersion;
    private WindowsLicenceTypes licenceType;

    public WindowsVersion(){

    }

    @Override
    public String getSpecifications(){
        StringBuilder specifications = new StringBuilder(
                "Produktname: " + getProductName() + "\n"
                + "Hersteller: " + getManufacturer() + "\n"
                + "Windows Version: " + windowsVersion + "\n"
                + "Lizenz-Typ: " + licenceType.name() + "\n"
        );
        return specifications.toString();
    }

    //Getters

    public String getWindowsVersion() {
        return windowsVersion;
    }

    public WindowsLicenceTypes getLicenceType() {
        return licenceType;
    }

    //Setters

    public void setLicenceType(WindowsLicenceTypes licenceType) {
        this.licenceType = licenceType;
    }

    public void setWindowsVersion(String windowsVersion) {
        this.windowsVersion = windowsVersion;
    }
}
