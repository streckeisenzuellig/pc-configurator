package configurator.data.models.types;

/**
 * Created by lukas on 05.05.2017.
 */
public enum ComponentTypes {
    CPU,
    CPU_COOLING,
    CASE,
    GRAPHIC_CARD,
    MAINBOARD,
    MONITOR,
    POWER_ADAPTER,
    RAM,
    HDD,
    SSD,
    WINDOWS
}
