package configurator.data.models.types;

/**
 * Created by Lukas_Streckeisen on 21.04.2017.
 */
public enum WindowsLicenceTypes {
    HOME,
    PRO,
    ENTERPRICE
}
