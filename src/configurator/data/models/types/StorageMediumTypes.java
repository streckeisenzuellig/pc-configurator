package configurator.data.models.types;

/**
 * Created by lukas on 07.04.2017.
 */
public enum StorageMediumTypes {
    HDD,
    SSD
}
