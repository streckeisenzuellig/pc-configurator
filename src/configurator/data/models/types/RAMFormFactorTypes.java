package configurator.data.models.types;

/**
 * Created by Lukas_Streckeisen on 21.04.2017.
 */
public enum RAMFormFactorTypes {
    DIMM_184,
    DIMM_240,
    DIMM_288,
    SODIMM_200,
    SODIMM_204,
    SO_DIMM_260_pin
}
