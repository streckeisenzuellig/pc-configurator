package configurator.data.models.types;

/**
 * Created by Lukas_Streckeisen on 21.04.2017.
 */
public enum FormFactorTypes {
    ATX,
    DTX,
    E_ATX,
    HPTX,
    mATX,
    Mini_DTX,
    Mini_ITX,
    SSI,
    Thin_Mini_ITX,
    XL_ATX

}
