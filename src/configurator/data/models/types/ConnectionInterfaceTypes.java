package configurator.data.models.types;

/**
 * Created by Lukas_Streckeisen on 21.04.2017.
 * wird für alle arten von anschlüssen benutzt
 */
public enum ConnectionInterfaceTypes {
    _3_pin,
    _4_pin_PWM,
    _24_pin_ATX,
    _8_pin_ATX_12V,
    PCI_E_x1,
    PCI_E_x16,
    PCI_E_8_pin,
    S_ATA_III,
    HDMI,
    DisplayPort,
    DVI_D,
    VGA,
    PS_2_COMBO,
    USB_3_0,
    USB_3_1,
    USB_TYPE_C
}
