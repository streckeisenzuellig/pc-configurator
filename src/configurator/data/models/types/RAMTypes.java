package configurator.data.models.types;

/**
 * Created by Lukas_Streckeisen on 21.04.2017.
 */
public enum RAMTypes {
    DDR2_RAM,
    DDR3L_RAM,
    DDR3_RAM,
    DDR3U_RAM,
    DDR4_RAM,
    DDR_RAM,
    SDRAM
}
