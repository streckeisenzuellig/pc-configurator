package configurator.data.models.types;

/**
 * Created by Lukas_Streckeisen on 21.04.2017.
 */
public enum StorageMediumSizeTypes {
    _2_5_inch,
    _3_5_inch
}
