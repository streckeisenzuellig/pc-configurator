package configurator.data.models.types;

/**
 * Created by Lukas_Streckeisen on 21.04.2017.
 */
public enum CPUBaseTypes {
    AM3_PLUS,
    AM4,
    C32,
    FCLGA2011,
    FM2,
    FM2_PLUS,
    LGA_1150,
    LGA_1151,
    LGA_1155,
    LGA_2011,
    LGA_2011_v3
}
