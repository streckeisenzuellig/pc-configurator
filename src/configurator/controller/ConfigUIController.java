package configurator.controller;

import configurator.AlertCreator;
import configurator.Main;
import configurator.data.models.configuration.Configuration;
import configurator.data.models.types.ComponentTypes;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * Created by lukas on 07.04.2017.
 */
public class ConfigUIController {

    @FXML
    private Label labConfigurationName;
    @FXML
    private Label labActualMainboard;
    @FXML
    private Label labActualRam;
    @FXML
    private Label labActualCpu;
    @FXML
    private Label labActualCase;
    @FXML
    private Label labActualCpuCool;
    @FXML
    private Label labActualGraphicCard;
    @FXML
    private Label labActualHdd;
    @FXML
    private Label labActualSsd;
    @FXML
    private Label labActualPowerAdapt;
    @FXML
    private Label labActualWinVersion;

    @FXML
    private Button butChooseMainboard;
    @FXML
    private Button butChooseRam;
    @FXML
    private Button butChooseCpu;
    @FXML
    private Button butChooseCase;
    @FXML
    private Button butChooseCpuCool;
    @FXML
    private Button butChooseGraphicCard;
    @FXML
    private Button butChooseHdd;
    @FXML
    private Button butChooseSsd;
    @FXML
    private Button butChoosePowerAdapt;
    @FXML
    private Button butChooseWinVersion;

    private Configuration configuration;

    public ConfigUIController(){

    }

    public void initUI(){
        if (configuration == null){
            configuration = new Configuration();
            labConfigurationName.setText("New Configuration");
            configuration.setConfigurationName("New Configuration");

            labActualCase.setText("");
            labActualCpu.setText("");
            labActualCpuCool.setText("");
            labActualGraphicCard.setText("");
            labActualHdd.setText("");
            labActualMainboard.setText("");
            labActualPowerAdapt.setText("");
            labActualRam.setText("");
            labActualSsd.setText("");
            labActualWinVersion.setText("");
        }
        else{

            labConfigurationName.setText(configuration.getConfigurationName());

            labActualWinVersion.setText(configuration.getSelectedWindowsVersion().getWindowsVersion());
            labActualSsd.setText(configuration.getSelectedSSD().getProductName());
            labActualRam.setText(configuration.getSelectedRAM().getProductName());
            labActualPowerAdapt.setText(configuration.getSelectedPowerAdapter().getProductName());
            labActualMainboard.setText(configuration.getSelectedMainboard().getProductName());
            labActualGraphicCard.setText(configuration.getSelectedGraphicCard().getProductName());
            labActualHdd.setText(configuration.getSelectedHDD().getProductName());
            labActualCpuCool.setText(configuration.getSelectedCPUCooling().getProductName());
            labActualCpu.setText(configuration.getSelectedCPU().getProductName());
            labActualCase.setText(configuration.getSelectedCase().getProductName());
        }
    }

    public void handleChoose(ActionEvent event){

        ComponentTypes componentType;

        if (event.getSource() == butChooseCase){
            componentType = ComponentTypes.CASE;
        }
        else if (event.getSource() == butChooseCpu){
            componentType = ComponentTypes.CPU;
        }
        else if (event.getSource() == butChooseCpuCool){
            componentType = ComponentTypes.CPU_COOLING;
        }
        else if (event.getSource() == butChooseGraphicCard){
            componentType = ComponentTypes.GRAPHIC_CARD;
        }
        else if (event.getSource() == butChooseHdd){
            componentType = ComponentTypes.HDD;
        }
        else if (event.getSource() == butChooseMainboard){
            componentType = ComponentTypes.MAINBOARD;
        }
        else if (event.getSource() == butChoosePowerAdapt){
            componentType = ComponentTypes.POWER_ADAPTER;
        }
        else if (event.getSource() == butChooseSsd){
            componentType = ComponentTypes.SSD;
        }
        else if (event.getSource() == butChooseRam){
            componentType = ComponentTypes.RAM;
        }
        else {
            componentType = ComponentTypes.WINDOWS;
        }

        Main.showSelectUI(configuration, componentType);
    }

    public void handleCancel(){
        Main.showMainUI();
    }

    public void handleSave(){
        if (
            configuration.getSelectedCase().getProductName() != null &&
            configuration.getSelectedCPU().getProductName() != null &&
            configuration.getSelectedCPUCooling().getProductName() != null &&
            configuration.getSelectedGraphicCard().getProductName() != null &&
            (
                    configuration.getSelectedHDD().getProductName() != null ||
                    configuration.getSelectedHDD().getProductName() != null
            ) &&
            configuration.getSelectedMainboard().getProductName() != null &&
            configuration.getSelectedPowerAdapter().getProductName() != null &&
            configuration.getSelectedRAM().getProductName() != null &&
            configuration.getSelectedWindowsVersion().getProductName() != null
        ) {
            //TODO: save the configuration
            Main.showMainUI();
        }
        else{
            AlertCreator.createAlert(Alert.AlertType.ERROR, "Konfiguration unvollständig", "Mindestens eine erforderliche Komponente fehlt", "Bitte wählen Sie alle erforderlichen Komponenten aus");
        }
    }

    public void setConfiguration(Configuration configuration){
        this.configuration = configuration;
    }
}
