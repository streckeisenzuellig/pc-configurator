package configurator.controller;

import configurator.Main;
import configurator.data.models.configuration.Configuration;
import configurator.data.models.configuration.components.*;
import configurator.data.models.types.ComponentTypes;
import configurator.data.models.types.StorageMediumTypes;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.Text;

import java.util.List;
import java.util.stream.Collectors;

public class SelectUIController {

    @FXML
    private ListView<Product> componentsList;
    @FXML
    private ScrollPane specificationsScrollPane;
    @FXML
    private Label labProductName;
    @FXML
    private ScrollPane descriptionScrollPane;

    private Configuration configuration;
    private ComponentTypes componentType;

    public SelectUIController(){
        componentsList = new ListView<>();
    }

    public void initUI(ComponentTypes componentType) {

        componentsList.setOnMouseClicked(
                event -> {
                    labProductName.setText("");
                    descriptionScrollPane.setContent(null);
                    specificationsScrollPane.setContent(null);
                    labProductName.setText(componentsList.getSelectionModel().getSelectedItem().getProductName());
                    Text t = new Text(componentsList.getSelectionModel().getSelectedItem().getDescription());
                    descriptionScrollPane.setContent(t);
                    Text text = new Text(componentsList.getSelectionModel().getSelectedItem().getSpecifications());
                    specificationsScrollPane.setContent(text);
                }
        );

        this.componentType = componentType;

        ObservableList<Product> products = FXCollections.observableArrayList();

        componentsList.setItems(products);

        switch (componentType) {
            case CPU:
                products.addAll(Main.dataHandler.getCPUs());
                break;
            case HDD:
                List<StorageMedium> storageMedia = Main.dataHandler.getStorageMediums();
                storageMedia = storageMedia.stream().filter(storageMedium -> storageMedium.getStorageMediumType() == StorageMediumTypes.HDD).collect(Collectors.toList());
                products.addAll(storageMedia);
                break;
            case RAM:
                products.addAll(Main.dataHandler.getRAMs());
                break;
            case SSD:
                storageMedia = Main.dataHandler.getStorageMediums();
                storageMedia = storageMedia.stream().filter(storageMedium -> storageMedium.getStorageMediumType() == StorageMediumTypes.SSD).collect(Collectors.toList());
                products.addAll(storageMedia);
                break;
            case CASE:
                products.addAll(Main.dataHandler.getCases());
                break;
            case MONITOR:
                products.addAll(Main.dataHandler.getMonitors());
                break;
            case WINDOWS:
                products.addAll(Main.dataHandler.getWindowsVersions());
                break;
            case MAINBOARD:
                products.addAll(Main.dataHandler.getMainboards());
                break;
            case CPU_COOLING:
                products.addAll(Main.dataHandler.getCPUCoolings());
                break;
            case GRAPHIC_CARD:
                products.addAll(Main.dataHandler.getGraphicCards());
                break;
            case POWER_ADAPTER:
                products.addAll(Main.dataHandler.getPowerAdapters());
                break;
            default:
                products.clear();
        }

        if (componentsList.getItems().size() > 0) {
            componentsList.getSelectionModel().selectFirst();
            labProductName.setText(componentsList.getSelectionModel().getSelectedItem().getProductName());
            Text t = new Text(componentsList.getSelectionModel().getSelectedItem().getDescription());
            descriptionScrollPane.setContent(t);
            Text text = new Text(componentsList.getSelectionModel().getSelectedItem().getSpecifications());
            specificationsScrollPane.setContent(text);
        }
        else {
            labProductName.setText("");
        }

    }

    public void handleCancel(){
        Main.showConfigUI(configuration);
    }

    public void handleSelect(){

        Product product = componentsList.getSelectionModel().getSelectedItem();

        switch (componentType) {
            case CPU:
                configuration.setSelectedCPU((CPU)product);
                break;
            case HDD:
                configuration.setSelectedHDD((StorageMedium) product);
                break;
            case RAM:
                configuration.setSelectedRAM((RAM)product);
                break;
            case SSD:
                configuration.setSelectedSSD((StorageMedium) product);
                break;
            case CASE:
                configuration.setSelectedCase((Case) product);
                break;
            case MONITOR:
                configuration.setSelectedMonitor((Monitor) product);
                break;
            case WINDOWS:
                configuration.setSelectedWindowsVersion((WindowsVersion) product);
                break;
            case MAINBOARD:
                configuration.setSelectedMainboard((Mainboard) product);
                break;
            case CPU_COOLING:
                configuration.setSelectedCPUCooling((CPUCooling) product);
                break;
            case GRAPHIC_CARD:
                configuration.setSelectedGraphicCard((GraphicCard) product);
                break;
            case POWER_ADAPTER:
                configuration.setSelectedPowerAdapter((PowerAdapter) product);
                break;
        }

        Main.showConfigUI(configuration);
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }
}
